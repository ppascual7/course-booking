
let token = localStorage["token"]
let apiCourseURL = "https://coursebooking-pascual.herokuapp.com/api/courses"
let apiUserURL = "https://coursebooking-pascual.herokuapp.com/api/users"

let tbody = document.querySelector('#tbody')
let appendChild = (value) => {

    let tbody = document.querySelector('#tbody')

    const tr = document.createElement("tr")
    tr.innerHTML = value
    tbody.appendChild(tr)

}

if(!token) {
    alert('You must login first!')
    window.location.replace('./login.html')
} else {

    let options = {
        headers: {
            "Content-Type": 'application/json',  
            "Authorization" : `Bearer ${token}`
        }
    }

    fetch(`${apiUserURL}/details`, options)
    .then( res => res.json())
    .then( data => {

        let enrolledCourses = data.enrollments;

        document.querySelector("#firstNameh3").innerHTML = `${data.firstName}`;
        document.querySelector("#lastNameh3").innerHTML = `${data.lastName}`;
        document.querySelector("#emailh3").innerHTML = `${data.email}`;

        if(enrolledCourses.length === 0){
            appendChild("No enrolled courses yet")
        } else {
            enrolledCourses.forEach( course => {

                fetch(`${apiCourseURL}/${course.courseId}`)
                .then( res => res.json())
                .then ( enrollments => {

                    let courseStatus = "Closed"
                    if(enrollments.isActive) courseStatus = "Active"

                    console.log(typeof(enrollments.isActive))
                    value = `
                            <td>${enrollments.name}</td>
                            <td>${new Date(course.enrolledOn)}</td>
                            <td>${courseStatus}</td>
                            <td>${course.status}</td>`
                    appendChild(value)
                })
            })
        }
    })
}
