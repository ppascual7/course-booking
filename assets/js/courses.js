let adminUser = localStorage["isAdmin"]
let cardFooter = "";
let url = 'https://coursebooking-pascual.herokuapp.com/api/courses/'

let currencyFormatter = new Intl.NumberFormat('en-US', {
	style: 'currency',
	currency: 'PHP',
  });

let updateCourseStatus = (body) => {

	options = {
		method: "PUT",
        body : JSON.stringify(body),
        headers :{
            "Content-Type" : "application/json",
            "Authorization" : `Bearer ${localStorage["token"]}`
        }
	}

	fetch(url, options)
	.then( res => res.json())
	.then( data => {

		if (!data) return alert("Error")

	}) 
}

let addEventListenerForToggle = (data) => {

	let toggle = document.querySelector(`.toggle-${data._id}`)
	let labelToggle = document.querySelector(`#label-toggle-${data._id}`)

	toggle.addEventListener("change", (e) => {
		if(toggle.checked){
			updateCourseStatus({_id: data._id, isActive: true})
			labelToggle.innerHTML = "Disable"
			iconColor = "green"
		} else {
			updateCourseStatus({_id: data._id, isActive: false})
			labelToggle.innerHTML = "Enable"
		}
	})
}

let setToggleInitialState = (data) => {
	
	let toggle = document.querySelector(`.toggle-${data._id}`)
	let labelToggle = document.querySelector(`#label-toggle-${data._id}`)

	if(data.isActive){
		toggle.checked = true
		labelToggle.innerHTML = "Disable"
	} else {
		toggle.checked = false
		labelToggle.innerHTML = "Enable"
	}
}

if (adminUser == "false" || !adminUser) {

	url = 'https://coursebooking-pascual.herokuapp.com/api/courses?isActive=true'

	adminButton.innerHTML = ""
	} else {
	adminButton.innerHTML = `
	<div class=add-course-container>
		<div class="admin-button">
			<a href="./addCourse.html">Add Course</a>
		</div>
	</div>
	`
	}

function displayCardFooter(courseId, index){

	if (adminUser == "false" || !adminUser) {
		cardFooter = `
						<div class="button-container">
							<a href="./course.html?courseId=${courseId}&ind=${index}">Details</a>
						</div>`
	} else {

		cardFooter = `
		<div class="admin-button-container">
			<div class="admin-button">
				<a href="./editCourse.html?courseId=${courseId}">Edit</a>
			</div>
			<div class="admin-button">
				<a href="./deleteCourse.html?courseId=${courseId}">Delete</a>
			</div>
			<div class="admin-button">
				<a href="./course.html?courseId=${courseId}&ind=${index}">Details</a>
			</div>
			<div class="form-check form-switch my-3">
				<input class="form-check-input toggle-${courseId}" type="checkbox" id="flexSwitchCheckDefault">
				<label style="color: gray" id="label-toggle-${courseId}" class="form-check-label" for="flexSwitchCheckDefault">Enable Course</label>
			</div>
		</div>`
		

	}

	return cardFooter
	
}

fetch(url)
.then( res => {
	return res.json()
})
.then( data => {

	let courseContainer = document.querySelector('#courseContainer');
	console.log(data)

	let courseData = data.map( (elem, index) => {

		console.log("index--", index+1)
		ind = "odd"
		console.log((index+1)%2)

		if ((index+1) % 2 === 0){
			ind="even"
			courseHTML = `
			<div class="row course-container darkblue-bg">
				<div class="col-md-12 col-lg-2"></div>

				<div class="col-md-12 col-lg-4 course-details-container-even" data-aos="fade-left">
					<div>	
						<h2 id="h1-even">${elem.name}</h2>
						<p id="span-even">${elem.description}</p>
						<p id="price-even">${currencyFormatter.format(elem.price)}</p>
					</div>
					${displayCardFooter(elem._id, (ind))}

				</div>
			
				<div class="col-md-12 col-lg-4 course-image-ph image-even" data-aos="fade-right">
					<img src="./../assets/images/IMG_1142.JPG">
				</div>

				<div class="col-md-12 col-lg-2"></div>

			</div>`
		}

		else { 
			courseHTML = `
			<div class="row course-container">

				<div class="col-md-12 col-lg-2"></div>
				<div class="col-md-12 col-lg-4 course-image-ph image-odd" data-aos="fade-left">
				<img src="./../assets/images/11707.jpg">
				</div>
				<div class="col-md-12 col-lg-4 course-details-container-odd" data-aos="fade-right">	  
					<div>
						<h2>${elem.name}</h2>
						<p id="span-odd">${elem.description}</p>
						<p id="price-odd">${currencyFormatter.format(elem.price)}</p>
					</div>
					${displayCardFooter(elem._id, (ind))}

				</div>

				<div class="col-md-12 col-lg-2"></div>

			</div>`
		}

		return courseHTML

	})

	courseContainer.innerHTML = courseData.join("");

	if(adminUser === "true"){
		
		data.forEach( data => {

			setToggleInitialState(data)
			addEventListenerForToggle(data)
			
		})
	} 
})

