let logInUser = document.querySelector('#logInUser');
let apiUserURL = "https://coursebooking-pascual.herokuapp.com/api/users"

function displayAlert(){
	let container = document.querySelector(".login-container-2")

	div = document.createElement("div")
	div.className = "alert alert-warning alert-dismissible fade show alert-login"
	div.role = "alert"
	div.innerHTML = `<strong>Oops!</strong> Email or Password is incorrect
	
	<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>`

	console.log(div)
	console.log(container)
	container.appendChild(div)
}

logInUser.addEventListener('submit', e =>{
	e.preventDefault();

	let email = document.querySelector('#email').value
	let password = document.querySelector('#password').value

	let url = `${apiUserURL}/login`
	let options = {
		method: "POST",
		body: JSON.stringify({
			email: email,
			password : password
		}),
		headers : {
			"Content-Type" : "application/json"
		}
	}

	fetch(url,options)
	.then( res => res.json())
	.then( data => {

		console.log(data)
		if (!data) return displayAlert();

		localStorage.setItem("token", data.accessToken)

		fetch(`${apiUserURL}/details`,{
			headers: {
				"Authorization" : `Bearer ${localStorage["token"]}`
			}
		})
		.then( res => res.json())
		.then( user =>{

			localStorage["id"] = user._id
			localStorage["isAdmin"] = user.isAdmin



			window.location.replace('./courses.html')
		})
	})
})