let courseId = new URLSearchParams(window.location.search).get('courseId');
let token = localStorage.getItem('token');
let apiCourseURL = "https://coursebooking-pascual.herokuapp.com/api/courses"

fetch(`${apiCourseURL}/${courseId}`, {
    method: 'DELETE',
    headers: {
        'Authorization': `Bearer ${token}`
    }
})
.then(res => res.json())
.then(data => {
    console.log(data)
})