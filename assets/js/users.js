
let token = localStorage["token"]
let apiUserURL = "https://coursebooking-pascual.herokuapp.com/api/users"

let tbody = document.querySelector('#tbody')

function displayAlert(id, role, type){
    let container = document.querySelector("#usersContainer")
    let message = `<strong>Setting role as ${role}</strong> for user ${id}`
    if(type === "delete"){
        message = `<strong>User: ${id}</strong> has been deleted`
    }

	div = document.createElement("div")
	div.className = "alert alert-warning alert-dismissible fade show alert-users"
	div.role = "alert"
	div.innerHTML = `${message}
	
	<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>`

	console.log(div)
	console.log(container)
    container.appendChild(div)

    console.log('div', div)
            
    let alert = document.querySelector(".btn-close")
    alert.addEventListener('click', function(){
        window.location.reload()
    })
}

let appendChild = (value) => {

    let tbody = document.querySelector('#tbody')

    const tr = document.createElement("tr")
    tr.innerHTML = value
    tbody.appendChild(tr)

}

//Alert event listener:


if(!isAdmin || isAdmin !== "true") {
    alert('Unauthorized Access')
    window.location.replace('./../index.html')
} else {

    let options = {
        headers: {
            "Content-Type": 'application/json',  
            "Authorization" : `Bearer ${token}`
        }
    }

    fetch(`${apiUserURL}`, options)
    .then( res => res.json())
    .then( data => {

        data.forEach( user => {

            let role="User"
            let setAs="Admin"
            if(user.isAdmin) {
                role="Admin" 
                setAs="User"
            }

            value = `
                    <td>${user.firstName} ${user.lastName}</td>
                    <td>${user.email}</td>
                    <td>${role}</td>
                    <td><a href="#" style="color: red" id=delete-user-${user._id}>Delete</td>
                    <td><a href="#" id=set-role-${user._id}>Set as ${setAs}</td>`
            appendChild(value)
            
            let deleteUser = document.querySelector(`#delete-user-${user._id}`)

            deleteUser.addEventListener('click', () => {
                console.log(`delete user--- ${user._id}`)

                fetch(`${apiUserURL}/${user._id}`, {
                    method: "DELETE",
                    headers: {
                        "Content-Type": 'application/json',  
                        "Authorization" : `Bearer ${token}`
                    }
                })
                .then( res => res.json())
                .then( data => {

                    if (!data) return alert("Something Went Wrong");
                    //alert(`${user._id} Successfully Deleted.`)
                    const type = "delete"
                    displayAlert(user._id, setAs, type)
                    //window.location.reload()
                })
            })

            let setRole = document.querySelector(`#set-role-${user._id}`)

            setRole.addEventListener('click', () => {
                console.log(`setting user--- ${user._id} as ${setAs}`)

                let body =  {
                    _id: user._id,
                    isAdmin: (!user.isAdmin)
                }
                
                fetch(apiUserURL, {
                    method: "PUT",
                    body: JSON.stringify(body),
                    headers: {
                        "Content-Type": 'application/json',  
                        "Authorization" : `Bearer ${token}`
                    }
                })
                .then( res => res.json())
                .then( data => {

                    if (!data) return alert("Something went wrong")

                    const type="set"
                    displayAlert(user._id, setAs, type)
                })
            })

        })
    })
}
