let apiUserURL = "https://coursebooking-pascual.herokuapp.com/api/users"

function displayAlert(){
	let container = document.querySelector("#registerForm")

	div = document.createElement("div")
	div.className = "alert alert-warning alert-dismissible fade show"
	div.role = "alert"
	div.innerHTML = `<strong>Something went wrong!</strong> Please recheck your inputs
	
	<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>`

	console.log(div)
	console.log(container)
	container.appendChild(div)
}

function displayAlertSuccess(){
	let container = document.querySelector("#registerForm")

	div = document.createElement("div")
	div.className = "alert alert-success alert-dismissible fade show"
	div.role = "alert"
	div.innerHTML = `<strong>Registration Successful!</strong>
	
	<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>`

	console.log(div)
	console.log(container)
	container.appendChild(div)

	let alert = document.querySelector(".alert")

	alert.addEventListener('close.bs.alert', function(){
        window.location.replace("./login.html")
    })

}

let registerForm = document.querySelector('#registerForm');
registerForm.addEventListener('submit', (e)=>{
	e.preventDefault();
	let firstName = document.querySelector('#firstName').value;
	let lastName = document.querySelector('#lastName').value;
	let email = document.querySelector('#email').value;
	let mobileNo = document.querySelector('#mobileNo').value;
	let password = document.querySelector('#password').value;
	let confirmPassword = document.querySelector('#confirmPassword').value;

	console.log(firstName,
		lastName,
		email,
		mobileNo,
		password,
		confirmPassword
	)

	let body = {
		firstName : firstName,
		lastName : lastName,
		email : email,
		mobileNo: mobileNo,
		password: password,
		confirmPassword : confirmPassword
	}

	fetch(apiUserURL,{
		method: "POST",
		body : JSON.stringify(body),
		headers :{
			"Content-Type" : "application/json"
		}
	})
	.then( res => res.json())
	.then( data => {
		if (data) {
			displayAlertSuccess();
		} else {
			displayAlert();
		}
	})

})