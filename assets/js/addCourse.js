
let createCourse = document.querySelector('#createCourse');

createCourse.addEventListener('submit', (e)=>{
    e.preventDefault();

    let name = document.querySelector('#name').value;
    let price = document.querySelector('#price').value;
    let description = document.querySelector('#description').value;

    let body = {
        name,
        price,
        description
    }

    const url = "https://coursebooking-pascual.herokuapp.com/api/courses/"

    const options = {
        method: "POST",
        body : JSON.stringify(body),
        headers :{
            "Content-Type" : "application/json",
            "Authorization" : `Bearer ${localStorage["token"]}`
        }
    }

    fetch(url, options)
    .then( res => res.json())
    .then( data => {

        console.log(data)

        if (data !== true ) return alert("Something went wrong")

        window.location.replace('./courses.html')
    })

})