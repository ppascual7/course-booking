const params = new URLSearchParams(window.location.search);
const courseId = params.get("courseId")

let apiCourseURL = `https://coursebooking-pascual.herokuapp.com/api/courses`

let courseName = document.querySelector("#courseName")
let courseDescription = document.querySelector("#courseDescription")
let coursePrice = document.querySelector("#coursePrice")

fetch(`${apiCourseURL}/${courseId}`)
.then(res => res.json())
.then(data => {
    courseName.value = data.name;
    courseDescription.value = data.description;
    coursePrice.value = data.price;
})

let editCourse = document.querySelector("#editCourse")
editCourse.addEventListener('submit', (e) => {

    e.preventDefault();

    let name = courseName.value;
    let description = courseDescription.value;
    let price = coursePrice.value;

    let body = {
        _id: courseId,
        name,
        price,
        description
    }

    let options = {
        method: "PUT",
        body : JSON.stringify(body),
        headers :{
            "Content-Type" : "application/json",
            "Authorization" : `Bearer ${localStorage["token"]}`
        }
    }

    console.log(options)

    fetch(apiCourseURL, options)
    .then(res => res.json())
    .then(data => {
        console.log(data)
        if (!data) return alert("Something went wrong")

        window.location.replace('./courses.html')

    }) 
})