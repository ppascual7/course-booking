const courseId = new URLSearchParams(window.location.search).get('courseId');
const index = new URLSearchParams(window.location.search).get('ind');

let token = localStorage["token"];
let apiCourseURL = "https://coursebooking-pascual.herokuapp.com/api/courses";
let apiUserURL = "https://coursebooking-pascual.herokuapp.com/api/users";

function displayAlert(){
	let container = document.querySelector("#courseDetailsContainer")

	div = document.createElement("div")
	div.className = "alert alert-warning alert-dismissible fade show"
	div.role = "alert"
	div.innerHTML = `<strong>Something went wrong!</strong>
	
	<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>`

	console.log(div)
	console.log(container)
	container.appendChild(div)
}

function displayAlertSuccess(){
	let container = document.querySelector("#courseDetailsContainer")

	div = document.createElement("div")
	div.className = "alert alert-success alert-dismissible fade show"
	div.role = "alert"
	div.innerHTML = `<strong>Thank you for enrolling!</strong>
	
	<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>`

	console.log(div)
	console.log(container)
	container.appendChild(div)

	let alert = document.querySelector(".alert")

	alert.addEventListener('close.bs.alert', function(){
        window.location.replace("./courses.html")
    })

}

let appendChild = (value) => {

    let tbody = document.querySelector('#tbody')

    const tr = document.createElement("tr")
    tr.innerHTML = value
    tbody.appendChild(tr)

}

let courseDetailsContainer = document.querySelector("#courseDetailsContainer")

let appendHeading = () => {

	const header = document.createElement("h3")
	header.innerHTML = "Enrolled Students"
	courseDetailsContainer.appendChild(header)

}

let appendTableContainer = (value) => {

	const table = document.createElement("table")
	table.className = "table table-hover"
	table.innerHTML = value
	courseDetailsContainer.appendChild(table)

}

let adminUser = localStorage["isAdmin"]

//Default picture display
if( index == "even" ){
	image = `<img src="./../assets/images/IMG_1142.JPG" alt="user">`
} else
	image = `<img src="./../assets/images/11707.jpg" alt="user">`

let img = document.querySelector(".course-picture-container")
img.innerHTML = image
	
//Not Admin
if( adminUser === "false" || !adminUser ){
	let url = `${apiCourseURL}/${courseId}`;

	fetch(url)
	.then(res => res.json())
	.then( data =>{

		let courseName = document.querySelector('#courseName')
		let courseDesc = document.querySelector('#courseDesc')
		let coursePrice = document.querySelector('#coursePrice')
		courseName.innerText = `Course Name: ${data.name}`
		courseDesc.innerText = `Description: ${data.description}`
		coursePrice.innerText = `Price: ${data.price}`

		document.querySelector("#enrollButton").addEventListener('click', () =>{

			const option = {
				method: "POST",
				headers: {
					'Content-Type' : 'application/json',
					'Authorization' : `Bearer ${token}`
				},
				body: JSON.stringify({
					_id : courseId
				})
			}

			fetch(`${apiUserURL}/enroll`,option)
			.then( res => res.json())
			.then( data => {
				
				console.log(data)

				if (data === true){
					displayAlertSuccess();
				} else{
					displayAlert();
				}
			})

		})
	})

// Admin
} else {

	let tableContainer = `
		<thead>
			<tr>
				<th> Name </th>
				<th> User ID </th>
				<th> Enrolled On </th>
			</tr>
		</thead>
		<tbody id="tbody">
		</tbody>
	`

	document.querySelector("#enrollContainer").remove();

	appendHeading()
	appendTableContainer(tableContainer)

	let url = `https://coursebooking-pascual.herokuapp.com/api/courses/${courseId}`
    let options = {
        headers: {
            "Content-Type": 'application/json',  
            "Authorization" : `Bearer ${token}`
        }
    }

    fetch(url, options)
    .then( res => res.json())
    .then( data => {

        enrollees = data.enrollees

		document.querySelector("#courseName").innerHTML = `Course: ${data.name}`;
		document.querySelector("#courseDesc").innerHTML = `Description: ${data.description}`;
		document.querySelector("#coursePrice").innerHTML = `Price: ${data.price}`;

        if( enrollees.length === 0 ) {
            appendChild("No students are enrolled in this course yet.")
        }
        else {

            enrollees.forEach(enrollees => {

				console.log(enrollees)

                fetch(`https://coursebooking-pascual.herokuapp.com/api/users/${enrollees.userId}`, {
                    headers: {
                        "Content-Type": 'application/json',  
                        "Authorization" : `Bearer ${token}`
                    }
                })
                .then( res => res.json())
                .then( userDetails => {

                    value = `
                        <td>${userDetails.firstName} ${userDetails.lastName}</td>
                        <td>${userDetails._id}</td>
                        <td>${new Date(enrollees.enrolledOn)}</td>
					`

                appendChild(value)

                }).catch( err => console.log(err))
            })
        }
    })
}

