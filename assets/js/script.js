let isAdmin = localStorage["isAdmin"]

let navbarContainer = document.querySelector("#regloginout_container")
let logoutPath = "logout.html"
let registerPath = "register.html"
let loginPath = "login.html"
let profilePagePath = "profile.html"
let usersPagePath = "users.html"


if (window.location.href.includes("index")){
    logoutPath = "pages/logout.html"
    loginPath = "pages/login.html"
    registerPath = "pages/register.html"
    profilePagePath = "pages/profile.html"
    usersPagePath = "pages/users.html"
}


if (!localStorage["token"]) {
    navbar = `
        <li class="nav-item">
            <a href="${registerPath}" class="nav-link">Register</a>
        </li>
        <li class="nav-item">
            <a href="${loginPath}" class="nav-link">Login</a>
        </li>`
} else {
    navbar = `
        <li class="nav-item">
            <a href="${profilePagePath}" class="nav-link">Profile</a>
        </li>
        <li class="nav-item">
            <a href="${logoutPath}" class="nav-link">Logout</a>
        </li>`
}

navbarContainer.innerHTML = navbar

let navBarMain = document.querySelector(".navbar-container")

if(isAdmin === "true"){

    usersButton = `<a href="${usersPagePath}" class="nav-link">Users</a>` 

    li = document.createElement("li")
    li.className = "nav-item"
    li.innerHTML = usersButton

    navBarMain.appendChild(li)
}

