
# Blackbook

An online learning platform mockup that lets users enroll to a specific course available. With admin CRUD functionality for overall content management. Guest and registered user page navigation with course enrollment restricted to registered users only.

## This Project

This is the UI code base of the blackbook app
